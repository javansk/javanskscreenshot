package ru.ilnitsky.nsk.java.screenshot;

import java.util.regex.Pattern;

/**
 * Параметры форматов изображений
 * Created by Mike on 20.04.2017.
 */
public enum ImageFormat {
    PNG("png", "PNG (*.png)", Pattern.compile("^.+\\.([pP][nN][gG])$")),
    GIF("gif", "GIF (*.gif)", Pattern.compile("^.+\\.([gG][iI][fF])$")),
    BMP("bmp", "BMP (*.bmp)", Pattern.compile("^.+\\.([bB][mM][pP])$")),
    JPG("jpg", "JPEG (*.jpg)", Pattern.compile("^.+\\.([jJ][pP][gG])$"));

    public final String extension;
    public final String description;
    public final Pattern pattern;

    ImageFormat(String extension, String description, Pattern pattern) {
        this.extension = extension;
        this.description = description;
        this.pattern = pattern;
    }
}
