package ru.ilnitsky.nsk.java.screenshot;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;

/**
 * Программа для создания снимка произвольного участка экрана
 * Created by Mike on 18.04.2017.
 */
public class ScreenShot extends JWindow {
    private class ImagePanel extends JPanel {
        private BufferedImage image;
        private float eps = 1e-3f;

        void setImage(BufferedImage image) {
            this.image = image;

            imageFrame.setSize(screenSize.width / 2, screenSize.height / 2);

            imageFrame.setVisible(true);
            int px = getWidth();
            int py = getHeight();
            imageFrame.setVisible(false);

            int ix = image.getWidth();
            int iy = image.getHeight();

            float panelCoefficient = px / (float) py;
            float imageCoefficient = ix / (float) iy;
            float d = panelCoefficient - imageCoefficient;

            if (d < -eps) {
                int y = imageFrame.getHeight() - (py - (int) (px / imageCoefficient));
                if (y < minHeight) {
                    y = minHeight;
                }
                imageFrame.setSize(imageFrame.getWidth(), y);
            } else if (d > eps) {
                int x = imageFrame.getWidth() - (px - (int) (py * imageCoefficient));
                if (x < minWidth) {
                    x = minWidth;
                }
                imageFrame.setSize(x, imageFrame.getHeight());
            }
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            if (image != null) {
                int px = getWidth();
                int py = getHeight();
                int ix = image.getWidth();
                int iy = image.getHeight();

                float panelCoefficient = px / (float) py;
                float imageCoefficient = ix / (float) iy;
                float d = panelCoefficient - imageCoefficient;

                if (Math.abs(d) < eps) {
                    g.drawImage(image, 0, 0, px, py, this);
                } else if (d < 0) {
                    int y = (int) (px / imageCoefficient);
                    g.drawImage(image, 0, 0, px, y, this);
                } else {
                    int x = (int) (py * imageCoefficient);
                    g.drawImage(image, 0, 0, x, py, this);
                }
            }
        }
    }

    private int minWidth = 320;
    private int minHeight = 160;

    private boolean isDragged;
    private boolean isLeftResize;
    private boolean isRightResize;
    private boolean isBottomResize;
    private boolean isTopResize;

    private Cursor cursorHand = new Cursor(Cursor.HAND_CURSOR);
    private Cursor cursorMove = new Cursor(Cursor.MOVE_CURSOR);
    private Cursor cursorLeft = new Cursor(Cursor.E_RESIZE_CURSOR);
    private Cursor cursorRight = new Cursor(Cursor.W_RESIZE_CURSOR);
    private Cursor cursorTop = new Cursor(Cursor.N_RESIZE_CURSOR);
    private Cursor cursorBottom = new Cursor(Cursor.S_RESIZE_CURSOR);

    private Point startWindowPosition;
    private Point startCursorPosition;
    private Dimension startWindowSize;
    private Dimension screenSize;

    private BufferedImage image;

    private JFileChooser fileChooser = new JFileChooser();

    private JFrame invisibleFrame = new JFrame();

    private JFrame menuFrame = new JFrame();
    private JPanel menuPanel = new JPanel();
    private JButton buttonFullScreen = new JButton("На весь экран");
    private JButton buttonLeftHalf = new JButton("Левая половина");
    private JButton buttonRightHalf = new JButton("Правая половина");
    private JButton buttonTopHalf = new JButton("Верхняя половина");
    private JButton buttonBottomHalf = new JButton("Нижняя половина");
    private JButton buttonQuarterScreen = new JButton("Четверть экрана");
    private JButton buttonAbout = new JButton("О программе");
    private JButton buttonExit = new JButton("Выход");

    private JFrame imageFrame = new JFrame();
    private ImagePanel imagePanel = new ImagePanel();
    private JPanel buttonPanel = new JPanel();
    private JButton buttonPNG = new JButton("PNG");
    private JButton buttonGIF = new JButton("GIF");
    private JButton buttonBMP = new JButton("BMP");
    private JButton buttonJPG = new JButton("JPG");

    private ScreenShot() {
        super();

        screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        setSize(screenSize.width / 2, screenSize.height / 2);
        setLocationRelativeTo(null);
        setOpacity(0.7f);
        setAlwaysOnTop(true);

        initMenuFrame();
        initImageFrame();

        Image icon = new ImageIcon(getClass().getResource("/ru/ilnitsky/nsk/java/screenshot/NskScreenShot.png")).getImage();
        menuFrame.setIconImage(icon);
        imageFrame.setIconImage(icon);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {

                    isDragged = true;
                    startWindowSize = getSize();
                    startWindowPosition = getLocationOnScreen();
                    startCursorPosition = e.getLocationOnScreen();

                    isDragged = true;
                    if (startCursorPosition.x >= startWindowPosition.x && startCursorPosition.x <= startWindowPosition.x + 5) {
                        isLeftResize = true;
                        isDragged = false;
                    } else if (startCursorPosition.x >= startWindowPosition.x + startWindowSize.width - 6
                            && startCursorPosition.x <= startWindowPosition.x + startWindowSize.width - 1) {
                        isRightResize = true;
                        isDragged = false;
                    } else if (startCursorPosition.y >= startWindowPosition.y && startCursorPosition.y <= startWindowPosition.y + 5) {
                        isTopResize = true;
                        isDragged = false;
                    } else if (startCursorPosition.y >= startWindowPosition.y + startWindowSize.height - 6
                            && startCursorPosition.y <= startWindowPosition.y + startWindowSize.height - 1) {
                        isBottomResize = true;
                        isDragged = false;
                    }

                    if (isDragged) {
                        setCursor(cursorMove);
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    isDragged = false;
                    isLeftResize = false;
                    isRightResize = false;
                    isTopResize = false;
                    isBottomResize = false;
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    try {
                        Robot robot = new Robot();
                        setVisible(false);

                        image = robot.createScreenCapture(new Rectangle(getX(), getY(), getWidth(), getHeight()));
                        if (image != null) {
                            showImageFrame();
                        } else {
                            setVisible(true);
                        }
                    } catch (AWTException ex) {
                        ex.printStackTrace();
                    }
                } else if (e.getButton() == MouseEvent.BUTTON3) {
                    showMenuFrame();
                }
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);

                int x = e.getX();
                int y = e.getY();
                if (x >= 0 && x <= 5) {
                    setCursor(cursorLeft);
                } else if (x >= getWidth() - 6 && x <= getWidth() - 1) {
                    setCursor(cursorRight);
                } else if (y >= 0 && y <= 5) {
                    setCursor(cursorTop);
                } else if (y >= getHeight() - 6 && y <= getHeight() - 1) {
                    setCursor(cursorBottom);
                } else {
                    setCursor(cursorHand);
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);

                if (isDragged || isLeftResize || isRightResize || isTopResize || isBottomResize) {
                    Point position = e.getLocationOnScreen();
                    int dx = position.x - startCursorPosition.x;
                    int dy = position.y - startCursorPosition.y;
                    if (isDragged) {
                        int x = startWindowPosition.x + dx;
                        int y = startWindowPosition.y + dy;
                        if (x < 0) {
                            x = 0;
                        } else if (x + getWidth() >= screenSize.getWidth()) {
                            x = (int) screenSize.getWidth() - getWidth();
                        }
                        if (y < 0) {
                            y = 0;
                        } else if (y + getHeight() >= screenSize.getHeight()) {
                            y = (int) screenSize.getHeight() - getHeight();
                        }
                        setLocation(x, y);
                    } else if (isLeftResize) {
                        setSize(startWindowSize.width - dx, startWindowSize.height);
                        setLocation(startWindowPosition.x + dx, startWindowPosition.y);
                    } else if (isRightResize) {
                        setSize(startWindowSize.width + dx, startWindowSize.height);
                    } else if (isTopResize) {
                        setSize(startWindowSize.width, startWindowSize.height - dy);
                        setLocation(startWindowPosition.x, startWindowPosition.y + dy);
                    } else if (isBottomResize) {
                        setSize(startWindowSize.width, startWindowSize.height + dy);
                    }
                }
            }
        });
    }

    private void initMenuFrame() {
        menuFrame.setTitle("NskScreenShot");
        menuFrame.setLocationRelativeTo(this);
        menuFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        menuFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                menuFrame.setVisible(false);
                setAlwaysOnTop(true);
                if (!isAlwaysOnTop()) {
                    setAlwaysOnTop(true);
                }
            }
        });

        menuFrame.add(menuPanel, BorderLayout.CENTER);

        menuPanel.setLayout(new GridLayout(8, 1));
        menuPanel.add(buttonFullScreen);
        menuPanel.add(buttonLeftHalf);
        menuPanel.add(buttonRightHalf);
        menuPanel.add(buttonTopHalf);
        menuPanel.add(buttonBottomHalf);
        menuPanel.add(buttonQuarterScreen);
        menuPanel.add(buttonAbout);
        menuPanel.add(buttonExit);

        buttonFullScreen.addActionListener(l -> {
            setVisible(false);
            setLocation(0, 0);
            setSize(screenSize.width, screenSize.height);
            setVisible(true);
            menuFrame.setVisible(true);
        });

        buttonLeftHalf.addActionListener(l -> {
            setVisible(false);
            setLocation(0, 0);
            setSize(screenSize.width / 2, screenSize.height);
            setVisible(true);
            menuFrame.setVisible(true);
        });

        buttonRightHalf.addActionListener(l -> {
            setVisible(false);
            setLocation(screenSize.width / 2, 0);
            setSize(screenSize.width / 2, screenSize.height);
            setVisible(true);
            menuFrame.setVisible(true);
        });

        buttonTopHalf.addActionListener(l -> {
            setVisible(false);
            setLocation(0, 0);
            setSize(screenSize.width, screenSize.height / 2);
            setVisible(true);
            menuFrame.setVisible(true);
        });

        buttonBottomHalf.addActionListener(l -> {
            setVisible(false);
            setLocation(0, screenSize.height / 2);
            setSize(screenSize.width, screenSize.height / 2);
            setVisible(true);
            menuFrame.setVisible(true);
        });

        buttonQuarterScreen.addActionListener(l -> {
            setVisible(false);
            setLocation(screenSize.width / 4, screenSize.height / 4);
            setSize(screenSize.width / 2, screenSize.height / 2);
            setVisible(true);
            menuFrame.setVisible(true);
        });

        buttonAbout.addActionListener(l ->
                JOptionPane.showMessageDialog(this,
                        new String[]{"NskScreenShot",
                                "Программа для создания снимков, выбранных участков экрана",
                                "Графический интерфейс на основе Swing",
                                "М.Ильницкий, Новосибирск, 2017"},
                        "О программе",
                        JOptionPane.INFORMATION_MESSAGE)
        );

        buttonExit.addActionListener(l -> {
            try {
                Object options[] = {"Да", "Нет"};
                int result = JOptionPane.showOptionDialog(this,
                        "Завершить программу?",
                        "Подтверждение завершения",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (result == JOptionPane.OK_OPTION) {
                    System.exit(0);
                }
            } catch (Exception exception) {
                exception.getStackTrace();
            }
        });
    }

    private void showMenuFrame() {
        setAlwaysOnTop(false);
        menuFrame.pack();
        menuFrame.setLocationRelativeTo(this);
        menuFrame.setVisible(true);
    }

    private void initImageFrame() {
        imageFrame.setTitle("Выберите формат для сохранения изображения");
        imageFrame.setMinimumSize(new Dimension(minWidth, minHeight));
        imageFrame.setLocationRelativeTo(null);
        imageFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        imageFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                imageFrame.setVisible(false);
                setVisible(true);
                if (!isVisible()) {
                    setVisible(true);
                }
            }
        });

        imageFrame.add(imagePanel, BorderLayout.CENTER);
        imageFrame.add(buttonPanel, BorderLayout.SOUTH);

        buttonPanel.setLayout(new GridLayout(1, 4));
        buttonPanel.add(buttonPNG);
        buttonPanel.add(buttonGIF);
        buttonPanel.add(buttonBMP);
        buttonPanel.add(buttonJPG);

        buttonPNG.addActionListener(l -> saveImageTo(ImageFormat.PNG));
        buttonGIF.addActionListener(l -> saveImageTo(ImageFormat.GIF));
        buttonBMP.addActionListener(l -> saveImageTo(ImageFormat.BMP));
        buttonJPG.addActionListener(l -> saveImageTo(ImageFormat.JPG));
    }

    private void showImageFrame() {
        setVisible(false);
        imagePanel.setImage(image);
        imageFrame.setLocationRelativeTo(null);
        imageFrame.setVisible(true);
    }

    private void saveImageTo(ImageFormat format) {
        if (image != null) {
            fileChooser.setDialogTitle("Сохранение снимка экрана");
            fileChooser.setApproveButtonText("Сохранить");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.resetChoosableFileFilters();
            fileChooser.setAcceptAllFileFilterUsed(false);

            FileNameExtensionFilter filter = new FileNameExtensionFilter(format.description, format.extension);
            fileChooser.setFileFilter(filter);

            int result = fileChooser.showSaveDialog(invisibleFrame);

            if (result == JFileChooser.APPROVE_OPTION) {
                try {
                    String fileName = fileChooser.getSelectedFile().getPath();
                    Matcher matcher = format.pattern.matcher(fileName);
                    if (!matcher.matches()) {
                        fileName += "." + format.extension;
                    }
                    ImageIO.write(image, format.extension, new File(fileName));
                } catch (IOException ex) {
                    ex.getStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        ScreenShot screenShot = new ScreenShot();
        screenShot.setVisible(true);
    }
}
